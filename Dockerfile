FROM gradle:6.6-jdk8 as builder
COPY . /home/gradle
RUN gradle build

FROM openjdk
COPY --from=builder /home/gradle/build/libs/JunitJacocoExample.jar /opt/JunitJacocoExample.jar

# This doesn't run a server so will exit
CMD ["java" "-jar" "/opt/JunitJacocoExample.jar"]
